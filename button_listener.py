import threading

import Jetson.GPIO as GPIO


class FaultInjectorButtonListener(threading.Thread):
    """
    This thread will listen the button events to implement the fault injection
    """
    __EVENT_WAIT_NORMAL_CONDITIONS = 0.3

    __EVENT_WAIT_FAULT_INJECTION = 5
    __BOARD_GPIO = 37

    def __init__(self, *args, **kwargs):
        self.__inject_now = False
        self.__event_stop = threading.Event()
        super(FaultInjectorButtonListener, self).__init__(*args, **kwargs)

        # Jetson GPIO
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.__BOARD_GPIO, GPIO.IN)

    def run(self) -> None:

        while True:
            gpio_status = GPIO.input(self.__BOARD_GPIO)
            if gpio_status == GPIO.HIGH:
                self.__inject_now = True
                self.__event_stop.wait(self.__EVENT_WAIT_FAULT_INJECTION)

            self.__inject_now = False

            if self.__event_stop.is_set():
                break
            self.__event_stop.wait(self.__EVENT_WAIT_NORMAL_CONDITIONS)
        # It must call cleanup only after the while stopped
        GPIO.cleanup()

    @property
    def inject_now(self) -> bool:
        return self.__inject_now

    def stop(self, timeout=None):
        self.__event_stop.set()
