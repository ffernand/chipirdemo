# chipirdemo



## Getting started

This repository is used to store the ISIS ChipIR demonstration tool.
All the scripts and tools to make the demo are contained here.

## Requirements

- Python >= 3.8
- OpenCV >= 4

