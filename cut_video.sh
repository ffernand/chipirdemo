#!/bin/bash

set -e
set -x

START=00:10:00
END=00:02:00

INPUT=data/London720p.mp4
OUTPUT=data/LondonShort.mp4

echo "Cutting video from ${START} to ${END}"
ffmpeg -ss ${START} -t ${END} -i ${INPUT} -vcodec copy -acodec copy ${OUTPUT} -y

echo "Video cut."

exit 0
