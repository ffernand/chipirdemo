#!/bin/bash

set -e
set -x

APP_ROOT_DIR=${HOME}/chipirdemo
DATA_PATH=${APP_ROOT_DIR}/data
INPUT_VIDEO=${DATA_PATH}/LondonShort.mp4
MODEL_WEIGHTS=${DATA_PATH}/yolov5n-seg.pt
LOGOS_FILE=${DATA_PATH}/logos.png

YOLOV5_PATH=${APP_ROOT_DIR}/yolov5

if [ ! -d "${YOLOV5_PATH}" ]; then
  echo "First download YOLOV5 from ultralytics and install the dependencies!"
  exit 404
fi

export PYTHONPATH=${YOLOV5_PATH}:${PYTHONPATH}

YAML_FILE=${YOLOV5_PATH}/data/coco128.yaml

${APP_ROOT_DIR}/main.py --video_file ${INPUT_VIDEO} \
                        --model_weights ${MODEL_WEIGHTS} \
                        --yaml_model_file ${YAML_FILE} \
                        --logos_file ${LOGOS_FILE}


exit 0