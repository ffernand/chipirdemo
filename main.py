#!/usr/bin/python
import argparse
import random
import time
import traceback
import typing
import cv2
import torch
import tqdm
import button_listener

from ultralytics.utils.plotting import Annotator, colors
from yolov5.models.common import DetectMultiBackend
from yolov5.utils.dataloaders import LoadImages
from yolov5.utils.general import check_img_size, non_max_suppression, scale_boxes
from yolov5.utils.segment.general import process_mask
from yolov5.utils.torch_utils import select_device

# Logger name in the main server thread
DEVICE = "cuda:0"
# Plural form for the coco classes
COCO_PLURAL_FORMS = {
    "person": "people", "bicycle": "bicycles", "car": "cars", "motorcycle": "motorcycles", "airplane": "airplanes",
    "bus": "buses", "train": "trains", "truck": "trucks", "boat": "boats", "traffic light": "traffic lights",
    "fire hydrant": "fire hydrants", "stop sign": "stop signs", "parking meter": "parking meters", "bench": "benches",
    "bird": "birds", "cat": "cats", "dog": "dogs",
    "horse": "horses", "sheep": "sheep", "cow": "cows", "elephant": "elephants", "bear": "bears", "zebra": "zebras",
    "giraffe": "giraffes",
    "backpack": "backpacks", "umbrella": "umbrellas", "handbag": "handbags", "tie": "ties", "suitcase": "suitcases",
    "frisbee": "frisbees", "skis": "skis", "snowboard": "snowboards", "sports ball": "sports balls", "kite": "kites",
    "baseball bat": "baseball bats", "baseball glove": "baseball gloves", "skateboard": "skateboards",
    "surfboard": "surfboards",
    "tennis racket": "tennis rackets", "bottle": "bottles", "wine glass": "wine glasses", "cup": "cups",
    "fork": "forks", "knife": "knives", "spoon": "spoons", "bowl": "bowls", "banana": "bananas", "apple": "apples",
    "sandwich": "sandwiches",
    "orange": "oranges", "broccoli": "broccoli", "carrot": "carrots", "hot dog": "hot dogs", "pizza": "pizzas",
    "donut": "donuts",
    "cake": "cakes", "chair": "chairs", "couch": "couches", "potted plant": "potted plants", "bed": "beds",
    "dining table": "dining tables", "toilet": "toilets", "tv": "tvs", "laptop": "laptops", "mouse": "mice",
    "remote": "remotes", "keyboard": "keyboards", "cell phone": "cell phones", "microwave": "microwaves",
    "oven": "ovens", "toaster": "toasters", "sink": "sinks",
    "refrigerator": "refrigerators", "book": "books", "clock": "clocks", "vase": "vases", "scissors": "scissors",
    "teddy bear": "teddy bears",
    "hair drier": "hair driers", "toothbrush": "toothbrushes"
}

FAULT_INJECTION_MIN_RANGE = 7
FAULT_INJECTION_MAX_RANGE = 15


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='ChipIR DNN demonstration.')
    parser.add_argument('--video_file', dest='video_file', required=True, help='Video file path')
    parser.add_argument('--model_weights', dest='model_weights', required=True, help='Model weights path')
    parser.add_argument('--yaml_model_file', dest='yaml_model_file', required=True, help='YAML file path')
    parser.add_argument('--logos_file', dest='logos_file', required=True, help='logos_file file path')

    args = parser.parse_args()
    print("Arguments:")
    for arg in vars(args):
        print(arg, getattr(args, arg))
    return args


@torch.inference_mode
def demo_chipir(
        weights: str, input_video_file: str, yaml_from_dataset: str, img_size: typing.Tuple[int, int],
        logos_image_path: str, num_of_frames: int, fault_injection_thread: button_listener.FaultInjectorButtonListener
) -> None:
    classes = None  # filter by class: --class 0, or --class 0 2 3
    agnostic_nms = False  # class-agnostic NMS
    augment = False  # augmented inference
    visualize = False  # visualize features
    line_thickness = 3  # bounding box thickness (pixels)
    hide_labels = False  # hide labels
    hide_conf = False  # hide confidences
    dnn = False  # use OpenCV DNN for ONNX inference
    vid_stride = 1  # video frame-rate stride
    conf_thres = 0.25  # confidence threshold
    iou_thres = 0.45  # NMS IOU threshold
    max_det = 1000  # maximum detections per image
    half_precision = True  # To execute in FP16

    # Load model
    device = select_device(DEVICE)
    model = DetectMultiBackend(weights, device=device, dnn=dnn, data=yaml_from_dataset, fp16=half_precision)
    stride, names, pt = model.stride, model.names, model.pt
    img_size = check_img_size(img_size, s=stride)  # check image size

    # Dataloader
    batch_size = 1
    dataset = LoadImages(input_video_file, img_size=img_size, stride=stride, auto=pt, vid_stride=vid_stride)

    # Run inference
    model.warmup(imgsz=(1 if pt else batch_size, 3, *img_size))  # warmup

    # Load everything in the memory
    print("Loading frames into the memory")
    input_list = list()
    num_of_frames -= 29  # The opencv and number of frames always differ by this number
    for _, im_host, im0s_host, _, _ in tqdm.tqdm(dataset, total=num_of_frames):
        im_device = torch.from_numpy(im_host).to(model.device).half().div(255.0).unsqueeze(0)
        input_list.append((im_device, im0s_host))
    # For the round counter
    frame_counter = 0
    num_of_frames = len(input_list)
    # font which we will be using to display FPS
    fps_print_font = cv2.FONT_HERSHEY_SIMPLEX
    tex_position = (2, 30)
    font_scale = 1
    font_color = (100, 255, 0)
    thickness = 2

    # Set the CV configs
    window_title = "Autonomous Car Demo"
    # cv2.namedWindow(window_title, cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)  # allow window resize (Linux)

    # Load the logos image
    logos_img = cv2.imread(logos_image_path)
    down_height, down_width, _ = logos_img.shape
    scale = 1.3
    down_points = (int(down_width / scale), int(down_height / scale))
    logos_img = cv2.resize(logos_img, down_points, interpolation=cv2.INTER_LINEAR)

    try:
        while True:
            im_device, im0s = input_list[frame_counter]
            frame_counter = (frame_counter + 1) % num_of_frames
            inference_process_time = time.time()

            if fault_injection_thread.inject_now:
                random_fi_value = random.randrange(FAULT_INJECTION_MIN_RANGE, FAULT_INJECTION_MAX_RANGE)
                pred, proto = model(im_device * random_fi_value, augment=augment, visualize=visualize)[:2]
            else:
                pred, proto = model(im_device, augment=augment, visualize=visualize)[:2]

            # NMS
            # with dt[2]:
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det, nm=32)

            inference_process_time = time.time() - inference_process_time
            assert len(pred) == 1, f"Number of predictions >1 --> {len(pred)}"
            post_process_time = time.time()

            # Process predictions
            det = pred[0]
            im0 = im0s.copy()

            annotator = Annotator(im0, line_width=line_thickness, example=str(names))
            print_string = ""
            if len(det):
                masks = process_mask(proto[0], det[:, 6:], det[:, :4], im_device.shape[2:], upsample=True)  # HWC
                det[:, :4] = scale_boxes(im_device.shape[2:], det[:, :4],
                                         im0.shape).round()  # rescale boxes to im0 size

                # Print results
                det_classes = det[:, 5].unique()
                for ci, c in enumerate(det_classes):
                    comma = ","
                    n = (det[:, 5] == c).sum()  # detections per class
                    class_name = names[int(c)] if n <= 1 else COCO_PLURAL_FORMS[names[int(c)]]
                    print_string += f"{n} {class_name}{comma * (ci < len(det_classes) - 1)} "  # add to string

                # Mask plotting
                annotator.masks(masks=masks, colors=[colors(x, True) for x in det[:, 5]], im_gpu=im_device[0])

                # Write results
                for j, (*xyxy, conf, cls) in enumerate(reversed(det[:, :6])):
                    # if save_img or save_crop or view_img:  # Add bbox to image
                    c = int(cls)  # integer class
                    conf_pct = int(conf * 100.0)
                    label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf_pct}%')
                    annotator.box_label(xyxy, label, color=colors(c, True))

            post_process_time = time.time() - post_process_time

            # Print time total
            delay_opencv = 10 if fault_injection_thread.inject_now else 1
            total_time = post_process_time + inference_process_time
            fps = int(1.0 / total_time)
            fps_string = f"FPS:{fps} - {print_string}"
            # putting the FPS count on the frame
            out_frame = annotator.result()

            cv2.putText(out_frame, fps_string, tex_position, fps_print_font, font_scale, font_color, thickness,
                        cv2.LINE_AA)
            # cv2.resizeWindow(window_title, out_frame.shape[1], out_frame.shape[0])
            cv2.imshow(window_title, out_frame)
            cv2.resizeWindow('Authors', logos_img.shape[1], logos_img.shape[0])
            cv2.imshow('Authors', logos_img)

            # This function is mandatory
            if fault_injection_thread.inject_now:
                # The delay must be higher to keep the video smooth
                cv2.waitKey(delay_opencv)
            else:
                cv2.waitKey(delay_opencv)
    except KeyboardInterrupt:
        print("CTRL-C pressed, finishing the demo!")
        cv2.destroyAllWindows()
        cv2.waitKey(1)


@torch.no_grad()
def main():
    # The flag below controls whether to allow TF32 on matmul. This flag defaults to False
    torch.backends.cuda.matmul.allow_tf32 = True

    # The flag below controls whether to allow TF32 on cuDNN. This flag defaults to True.
    torch.backends.cudnn.allow_tf32 = True

    # Get all args first
    args: argparse.Namespace = parse_args()
    video_path: str = args.video_file
    model_weights: str = args.model_weights
    yaml_model_file: str = args.yaml_model_file
    logos_image_path: str = args.logos_file

    # Start the listener thread right in the beginning
    button_listener_obj = button_listener.FaultInjectorButtonListener()
    button_listener_obj.start()
    cap = cv2.VideoCapture(video_path)
    # Check if camera opened successfully
    if cap.isOpened() is False:
        raise FileNotFoundError(f"could not open: {video_path}")

    num_of_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    size_img = (frame_width, frame_height)

    demo_chipir(weights=model_weights, input_video_file=video_path, yaml_from_dataset=yaml_model_file,
                img_size=size_img, num_of_frames=num_of_frames, logos_image_path=logos_image_path,
                fault_injection_thread=button_listener_obj)

    button_listener_obj.stop()
    button_listener_obj.join()
    print("DNN demo finished")


if __name__ == '__main__':
    try:
        main()
    except Exception as exception:
        with open("/home/chipir/exception_output.log", "w") as fp:
            exception_output = traceback.format_exception(exception.__class__, exception, exception.__traceback__)
            exception_trace = "\n".join(exception_output)
            print(exception_trace)
            fp.write(exception_trace)
