import torchvision
import cv2
import torch

def preprocess_frame(frame, device):
    transform = torchvision.transforms.ToTensor()
    input_frame_color = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    tensor_input = torch.unsqueeze(transform(input_frame_color), 0).to(device=device)
    return tensor_input
